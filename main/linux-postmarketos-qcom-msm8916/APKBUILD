# Maintainer: Minecrell <minecrell@minecrell.net>
# Kernel config based on: arch/arm64/configs/msm8916_defconfig

_flavor="postmarketos-qcom-msm8916"
pkgname=linux-$_flavor
pkgver=5.11_rc5
pkgrel=0
pkgdesc="Mainline kernel fork for Qualcomm MSM8916 devices"
arch="aarch64 armv7"
url="https://github.com/msm8916-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
makedepends="bison findutils flex installkernel openssl-dev perl gmp-dev mpc1-dev mpfr-dev"

# Architecture
case "$CARCH" in
	aarch64) _carch="arm64" ;;
	arm*)    _carch="arm" ;;
esac

# Source
_tag=v${pkgver//_/-}-msm8916
source="
	$pkgname-$_tag.tar.gz::$url/archive/$_tag.tar.gz
	config-$_flavor.aarch64
	config-$_flavor.armv7
"
builddir="$srcdir/linux-${_tag#v}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="16524ab5b77a17b2092d5d4537246e19bde9659fde24e922e9a7fb4052b61e2d1e4bf2d3ae8b1af540fb983c702a95c4209d0e1a1046ae6848119d31b01df31f  linux-postmarketos-qcom-msm8916-v5.11-rc5-msm8916.tar.gz
b2b0f5785deca9d7d7460bcd86504ffc1ded4b17b62abd92b6046854f25151d178019dc9456c9ba895e6cb8673bee4b08f9637093cdd28712188e332da12949b  config-postmarketos-qcom-msm8916.aarch64
72eb49458486a3d763cbdf4bb09e4a3a48133beef7a467d677dfb22cd506188ba689af237323e79d9096dd0640dd81ffbc8710d4c98427776ea35defdfb5ed72  config-postmarketos-qcom-msm8916.armv7"
