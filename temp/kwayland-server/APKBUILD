# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland-server
pkgver=5.20.90
pkgrel=0
pkgdesc="Wayland Server Components built on KDE Frameworks"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends_dev="
	kwayland-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	linux-headers
	qt5-qttools-dev
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-server-$pkgver.tar.xz"
subpackages="$pkgname-dev"
options="!check" # Requires running wayland compositor

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="027aff53b57022857b0ec63178f64b55f0f8ba18789fa3edaa09a3be06f32f3a75031c787ec8e6d77beac6878dba88bc0a25fbb881246c2c74ed7e005164dca0  kwayland-server-5.20.90.tar.xz"
